<?php
include("../lib/bigcheck.inc");
ini_set('filter.default', 'full_special_chars');
ini_set('filter.default_flags', 0);

$paramMap = array(
	'achternaam' => 'LastName',
	'aanhef' => 'Gender',
	'geboortedatum' => 'BirthDate',
	'voorletters' => 'Initial',
	'meisjesnaam' => 'MaidenName',
	'bignummer' => 'RegistrationNumber',
	'beroepsgroep' => 'Profession',
	'specialisme' => 'Specialism',
	'tussenvoegsels' => 'Prefix',
	'aantekening' => 'RegistrationProvisionNote',
);

$queryMap = array_flip($paramMap);
$queryMap['Gender'] = 'geslacht';

$paramMap = array_flip($queryMap);

$queryData = array();
foreach(array_keys($paramMap) as $param) {
	if (!empty($_REQUEST[$param])) {
		$queryData[$paramMap[$param]] = (isset($_REQUEST['encoding']) && $_REQUEST['encoding'] == 'latin1') ? utf8_encode($_REQUEST[$param]) : $_REQUEST[$param];
	}
}

if (isset($queryData['geboortedatum']) && ($queryData['geboortedatum']  == 'LEEG')) {
	exit;
}

if (!empty($queryData['Gender'])) {
	$queryData['Gender'] = in_array($queryData['Gender'],array('heer','Heer','m','M','mannelijk','male','man')) ? 'M' : 'V';
}

$bigcheck = new BigCheck();
$status = $bigcheck->check($queryData);
$profiles = $bigcheck->getProfiles();

$dbh = new PDO("mysql:dbname=".$_SERVER['db_name'].";host=".$_SERVER['db_host'],$_SERVER['db_user'],$_SERVER['db_passwd']);
$sth = $dbh->prepare('SELECT username FROM crm_user WHERE registrationId = :bignr');

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<title>Bigcheck</title>
<style>
	.error {margin-left:20px;font-size:2em;}

	div.bigData {
		max-width: 50%;
        min-width: 400px;
        width: auto !important;
        width: 400px;
        min-height: 260px;
        height: auto !important;
        height: 260px;
		float:left;
	}

	.bigData ul{list-style-type:none;}

	.bigData li{
		background-color:lightblue;
		font-style:italic;
		padding:3px 15px 3px 165px;
		margin-top:5px;
	}

	.bigData label{
		margin-right:10px;
		margin-left:-150px;
		font-style:normal;
		font-weight:bold;
		width:140px;
		float:left;
		text-transform:capitalize;
	}
	input {margin-left:10px;}
</style>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
</head>
<body>

	<form action="<?php echo $_SERVER['PHP_SELF'] ?>">
		<div class="queryData">
<?php
 /*
	foreach($queryData as $key => $value) {
		$value = filter_var($value);
		switch ($key) {
			case 'MaidenName':
				echo '<label for="maidenName">Gezocht op meisjesnaam:</label>';
				echo '<input type="text" id="maidenName" name="'. $queryMap[$key] .'" value="'. $value .'" onChange="this.form.submit();"/>';
				continue 2;
				break;
			case 'LastName':
				if (empty($queryData['MaidenName'])) {
					echo '<label for="lastName">Gezocht op achternaam:</label>';
					echo '<input type="text" id="lastName" name="'. $queryMap[$key] .'" value="'. $value .'" onChange="this.form.submit();"/>';
					continue 2;
				}
				break;
		}

		echo '<input type="hidden" name="'. $queryMap[$key] .'" value="'. $value .'"/>';
	}
*/
?>
		</div>
	</form>
<?php
	if ($status === false) {
		echo '<p class="error">Niet gevonden in bigregister.</p>';
	} elseif ($status === NULL) {
		echo '<p class="error">Owjee! Kan geen contact maken met het bigregister.</p>';
	}

	foreach ($profiles as $profile) {
		echo '<div class="bigData"><ul>';

		foreach($profile as $item => $value) {
			if ($item == 'Big') {
				foreach ($value as $bigData) {
					$sth->execute(array(':bignr' => $bigData['RegistrationNumber']));
					$mediAccess = $sth->fetch(PDO::FETCH_OBJ);
					echo '<li><label>'. $queryMap['RegistrationNumber'] .'<span>:</span></label>'. $bigData['RegistrationNumber'];
					echo '<br/><label>'. $queryMap['Profession'] .'<span>:</span></label>'. $bigData['Profession'];

					if (!empty($bigData['Specialism'])){
						echo '<br /><label>'. $queryMap['Specialism'] .'<span>:</span></label>'. $bigData['Specialism'];
					}

					if (!empty($mediAccess)) {
						echo '<br/><label>Gebruikersnaam<span>:</span></label>'. $mediAccess->username;
					}

					echo '</li>';
				}
				continue;
			}

			if ($item == 'Gender') {
				$value = ($value == 'M')  ? 'Man' : 'Vrouw';
			}

			echo '<li><label>'. $queryMap[$item] .'<span>:</span></label>'. filter_var($value) .'</li>';
		}
		echo '</ul></div>';
	}
?>
</body>
</html>
