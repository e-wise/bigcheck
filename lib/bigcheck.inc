<?php
/*
Author - Karel van IJperen
Date   - 2013-03-26
Version - 1.0-beta1

Usage1 - Get the full and all profilles matching your $queryData.

	Short method (without error checking):
		$bigcheck = new BigCheck($queryData);
		$profiles = bigcheck->getProfiles();

	Long(3 steps) method or repeated with diffrent data:
		$bigcheck = new BigCheck(); // 1.
		$ok = $bigcheck->check($queryData);

		if ($ok){
			$profiles = $bigcheck->getProfiles(); // 3.
		} elseif ($ok === NULL) {
			die ...; // Error occured in soap request
		} else {
			error("not found");
		}

Usage2 - If you only want to get the Big registration number and only want it if there is one result.
	$bignr = BigCheck::getSingleBig($queryData);

	if ($bignr){
			return $bignr;
	} elseif ($bignr === NULL) {
			die ...; // Error occured in soap request
	} else {
			error("not found");
	}

--- Example input (QueryData) ---
$queryData = array(
	'RegistrationNumber' => '19016457430',
	'LastName' => 'IJperen',
	'Prefix' => 'van',
	'Initial' => 'H.J.K.',
	'MaidenName' => '',
	'BirthDate' => '1984-07-24', // Or any other format strtotime accepts
	'Gender' => 'M', // M or V mind the capitals
	'Profession' => 'Verpleegkundige',
	'Specialism' => 'Verpl. spec. chronische zorg bij som. aandoeningen',
);

--- Example output (Profile) ---
$profiles[] = array(
	'LastName' => 'IJperen',
	'Prefix' => 'van',
	'Initial' => 'H.J.K.',
	'BirthDate' => '1984-07-24', // Or any other format strtotime accepts
	'Gender' => 'M', // M or V mind the capitals
	'Big' => array(
		array(
		'RegistrationNumber' => '19016457430',
		'Profession' => 'Verpleegkundige',
		'Specialism' => 'Verpl. spec. chronische zorg bij som. aandoeningen',
		),
	),
	'Limitation' => 'Suspended',
);
*/


class BigCheck {
	protected $profiles = array();
	private $queryData = array();

	public function __construct($queryData = array()) {
		if (!empty($queryData)){
			$this->check($queryData);
		}
	}

	public function check($queryData) {
		$this->profiles = array(); // Make sure we don't see data from the last check
		$this->queryData = $this->fixQueryData($queryData);
		$bigData = $this->getBigData($queryData);

		if ($bigData === NULL) { // Encounterd error
			return NULL;
		}

		if (empty($bigData)) {
		  return false;
		}

		$this->setProfiles($bigData);

		return true;
	}

	/*
	 * Returns an array like the query data, the difference is in the Big array
	 */
	public function getProfiles() {return $this->profiles;}

	static public function getSingleBig($queryData) {
		BigCheck::fixQueryData($queryData);
		$bigData = BigCheck::getBigData($queryData);
		if ($bigData === NULL) { // Encounterd error
			return NULL;
		}

		if(is_array($bigData)){
			return false;
		}

		$registration = $bigData->ArticleRegistration
			->ArticleRegistrationExtApp;

		if (is_array($registration)) {
			return false;
		}

		return BigCheck::zeroPad($registration->ArticleRegistrationNumber);
	}

	static private function fixQueryData(&$queryData) {
		array_walk($queryData, create_function('&$val', '$val = trim($val);'));
		if (!empty($queryData['BirthDate'])){
			$queryData['BirthDate'] = date('Y-m-d',strtotime($queryData['BirthDate']));
		}
		return $queryData;
	}

	static private function zeroPad($bignr) {
	  while(strlen($bignr) < 11) {
	    $bignr = "0" . $bignr;
	  }
	  return $bignr;
	}

	static public function getBigData($queryData){

		$soapData = array();

		$soapDataMap = array(
		  'Gender' => 'Gender',
		  'BirthDate' => 'DateOfBirth',
		  'Initial' => 'Initials',
		  'Profession' => 'ProfessionalGroup',
		  'Specialism' => 'TypeOfSpecialism',
		  'RegistrationNumber' => 'RegistrationNumber',
		);

		foreach($soapDataMap as $qdKey => $sdKey) {
			if (!empty($queryData[$qdKey])) {
				if (($qdKey == 'Profession') && !is_numeric($queryData[$qdKey])) {
					$queryData[$qdKey] = BigCheck::resolveProfession($queryData[$qdKey]);
				}

				if (($qdKey == 'Specialism') && !is_numeric($queryData[$qdKey])) {
					$queryData[$qdKey] = BigCheck::resolveSpecialism($queryData[$qdKey]);
				}

				$soapData[$sdKey] = $queryData[$qdKey];
			}
		}

		if (!empty($queryData['Prefix'])) {
			$queryData['LastName'] = $queryData['Prefix'] . ' ' . $queryData['LastName'];
		}

		// We need a name...
		if ( empty($queryData['MaidenName']) && empty($queryData['RegistrationNumber']) && empty($queryData['LastName']) ) {
			return NULL; //FIXME: NULL or false
		}


		$soapData['Name'] = empty($queryData['MaidenName']) ? $queryData['LastName'] : $queryData['MaidenName'];
		$bigData = BigCheck::doSoapRequest($soapData);
		if (empty($bigData) && !empty($queryData['MaidenName'])) {
			$soapData['Name'] = $queryData['LastName'];
			$bigData = BigCheck::doSoapRequest($soapData);
		}

		if ( empty($bigData) && empty($queryData['MaidenName']) &&
			(!isset($soapData['Gender']) || ($soapData['Gender'] == 'V'))
		){
			$matches = array();
			if ( preg_match('/[[:alnum:]]+-(.*)/u', $queryData['LastName'], $matches) ) {
				$soapData['Name'] = $matches[1];
				$bigData = BigCheck::doSoapRequest($soapData);
			}
		}
		return $bigData;
	}

	static private function doSoapRequest($soapData) {
	  $soapData['WebSite'] = 'Ribiz';
	  try {
		$original = ini_get('default_socket_timeout');
		ini_set('default_socket_timeout', 10);
		$url = 'https://api.bigregister.nl/zksrv/soap/4?WSDL';
		if(time() < strtotime('2023-04-25')) {
			$url = 'https://webservices.cibg.nl/Ribiz/OpenbaarV4.asmx?WSDL';
		}
	    $soap = @new SoapClient($url, array("exceptions" => 1));
	    $result = $soap->ListHcpApprox4($soapData);
		ini_set('default_socket_timeout', $original);
	  }
	  catch (Exception $e) {
	    return NULL;
	  }
	  return (count(get_object_vars($result->ListHcpApprox)) > 0) ? $result->ListHcpApprox->ListHcpApprox4 : array();
	}

	/*
	 * Fill the profile with te apropriate data from $bigData
	 */
	private function setProfiles($bigData){
		$profiles = array();
		if (!is_array($bigData)) {
			$bigData = array($bigData);
		}
		foreach ($bigData as $person) {
			$specialisms = array();
			$profile = array();
			$fieldOrder = array(
				'Initial',
				'Prefix',
				'LastName',
				'BirthDate',
				'Gender',
				'Big',
				'RegistrationProvisionNote',
			);

			$lastName = substr($person->MailingName,strlen($person->Initial)+1);
			if (!empty($person->Prefix)) {
				if ($person->Prefix . " " . $person->BirthSurname == $lastName) {
					$lastName = $person->BirthSurname;
				}
				else {
					$person->Prefix = "";
				}
			}

			foreach($fieldOrder as $fieldName){
				switch($fieldName) {
					case 'LastName':
						$profile[$fieldName] = $lastName;
						break;
					case 'BirthDate':
						if (!empty($this->queryData[$fieldName])) {
							$profile[$fieldName] = $this->queryData[$fieldName];
						}
						break;
					case 'Big':
						$profile[$fieldName] = array();
						break;
					case 'RegistrationProvisionNote':
						if (!empty($person->JudgmentProvision
							->JudgmentProvisionExtApp
							->PublicDescription
						)) {
							$profile[$fieldName] = $person->JudgmentProvision
								->JudgmentProvisionExtApp
								->PublicDescription;
						}
						break;
					default:
					  if (!empty($person->{$fieldName}) && !is_object($person->{$fieldName})) {
							$profile[$fieldName] = $person->{$fieldName};
						}
						break;
				}
			}

			if (!empty($person->Specialism->SpecialismExtApp1)) {
				$specialisms = is_array($person->Specialism->SpecialismExtApp1) ?
					$person->Specialism->SpecialismExtApp1 :
					array($person->Specialism->SpecialismExtApp1);
			}

			$registrations = is_array($person->ArticleRegistration->ArticleRegistrationExtApp) ?
				$person->ArticleRegistration->ArticleRegistrationExtApp :
				array($person->ArticleRegistration->ArticleRegistrationExtApp);

			foreach ($registrations as $registration ) {
				$big = array(
					'RegistrationNumber' => $this->zeroPad($registration->ArticleRegistrationNumber),
					'Profession' => $this->resolveProfession($registration->ProfessionalGroupCode),
				);

				foreach ($specialisms as $specialism) {
					if ($specialism->ArticleRegistrationNumber == $registration->ArticleRegistrationNumber){
						$big['Specialism'] = $this->resolveSpecialism($specialism->TypeOfSpecialismId);
						break;
					}
				}

				$profile['Big'][] = $big;
			}
			$profiles[] = $profile;

		}
		$this->profiles = $profiles;
	}

	/*
	 * Check $queryData and remove(do not add) anything that does not match
	 */
	private function filterProfiles() {
		$profiles = array();
		foreach ($this->profiles as $profile) {
			if ($this->checkProfile($profile)) {
				$profiles[] = $profile;
			}
		}
		$this->profiles = $profiles;
	}

	private function checkProfile($profile) {
		foreach ($this->queryData as $key => $value) {
			switch ($key) {
				case 'LastName':
				case 'MaidenName':
				case 'BirthDate':
					continue 2; //PHP thinks switch is a loop too O_o
					break;
				case 'Profession':
					foreach ($profile['Big'] as $bigData) {
						if ($bigData['Profession'] == $value) {
							continue 3;
						}
					}
					return false;
					break;
				case 'Specialism':
					foreach ($profile['Big'] as $bigData) {
						if ($bigData['Specialism'] == $value) {
							continue 3;
						}
					}
					return false;
					break;
			}

			if (!empty($value) && $profile[$key] != $value) {
				return false;
			}
		}
		return true;
	}

	static public function resolveProfession($profession) {
		static $professions = array(
			'01' => 'Arts',
			'02' => 'Tandarts',
			'03' => 'Verloskundige',
			'04' => 'Fysiotherapeut',
			'16' => 'Psychotherapeut',
			'17' => 'Apotheker',
			'25' => 'Gezondheidszorgpsycholoog',
			'30' => 'Verpleegkundige',
		);

		if (is_numeric($profession)) {
			return $professions[$profession];
		} //Else

		return array_search($profession, $professions);
	}

	static public function resolveSpecialism($specialism) {
		static $specialisms = array(
			'02' => 'Allergologie (allergoloog)',
			'03' => 'Anesthesiologie(anesthesioloog)',
			'04' => 'Huisartsgeneeskunde met apotheek (Apoth. Huisarts)',
			'08' => 'Arbeid en gezond - bedrijfsgeneeskunde',
			'10' => 'Cardiologie (cardioloog)',
			'11' => 'Cardio-thoracale chirurgie',
			'12' => 'Dermatologie en venerologie (dermatoloog)',
			'13' => 'Maag-darm-leverziekten (maag-darm-leverarts)',
			'14' => 'Heelkunde (chirurg)',
			'15' => 'Huisartsgeneeskunde (huisarts)',
			'16' => 'Interne geneeskunde (internist)',
			'18' => 'Keel-, neus- en oorheelkunde (kno-arts)',
			'19' => 'Kindergeneeskunde (kinderarts)',
			'20' => 'Klinische chemie (arts klinische chemie)',
			'21' => 'Klinische genetica (klinisch geneticus)',
			'22' => 'Klinische geriatrie (klinisch geriater)',
			'23' => 'Longziekten en tuberculose (longarts)',
			'24' => 'Medische microbiologie (arts-microbioloog)',
			'25' => 'Neurochirurgie (neurochirurg)',
			'26' => 'Neurologie (neuroloog)',
			'30' => 'Nucleaire geneeskunde (nucleair geneeskundige)',
			'31' => 'Oogheelkunde (oogarts)',
			'32' => 'Orthopedie (orthopeed)',
			'33' => 'Pathologie (patholoog)',
			'34' => 'Plastische chirurgie (plastisch chirurg)',
			'35' => 'Psychiatrie (psychiater)',
			'39' => 'Radiologie (radioloog)',
			'40' => 'Radiotherapie(radiotherapeut)',
			'41' => 'Reumatologie (reumatoloog)',
			'42' => 'Revalidatiegeneeskunde(revalidatiearts)',
			'43' => 'Maatschappij en gezondheid (beëindigd per 01-01-2007)',
			'45' => 'Urologie (uroloog)',
			'46' => 'Obstetrie en gynaecologie (gynaecoloog)',
			'47' => 'Specialismeouderengeneeskunde',
			'48' => 'Arbeid en gezondheid - verzekeringsgeneeskunde',
			'50' => 'Zenuw- en zielsziekten (zenuwarts)',
			'53' => 'Dento-maxillaire orthopaedie (orthodontist)',
			'54' => 'Mondziekten en kaakchirurgie (kaakchirurg)',
			'55' => 'Maatschappij en gezondheid',
			'56' => 'Geneeskunde voor verstandelijk gehandicapten',
			'60' => 'Ziekenhuisfarmacie(ziekenhuisapotheker)',
			'61' => 'Klinische psychologie (klinisch psycholoog)',
			'62' => 'Interne geneeskunde-allergologie',
			'63' => 'Klinische neuropsychologie',
			'65' => 'Verpl. spec. prev. zorg bij som. aandoeningen',
			'66' => 'Verpl. spec. acute zorg bij som. aandoeningen',
			'67' => 'Verpl. spec. intensieve zorg bij som. aandoeningen',
			'68' => 'Verpl. spec. chronische zorg bij som. aandoeningen',
			'69' => 'Verpl. spec. geestelijke gezondheidszorg',
			'70' => 'Jeugdgezondheidszorg (Profiel KNMG Jeugdarts)',
			'71' => 'Spoedeisendehulp (Profiel SEH Arts KNMG)',
			'74' => 'Sportgeneeskunde',
			'75' => 'Openbaar apotheker',
		);

		if (is_numeric($specialism)) {
			return $specialisms[$specialism];
		} //Else

		return array_search($specialism, $specialisms);
	}

}

?>
